
<!DOCTYPE html>
<html>
<head>
	<title>Vipin Chand (web Developer)</title>
	<link rel="stylesheet" href="bower_components/reset-css/reset.css">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="style.css">	
</head>
<body>
<div class="">
	<section class="bg-intro">
		<div class="container">
			<h3 class="text-center logo">Logo</h3>
			<div class="row text-center">
				<a href="" class="logo-tagline">
					<span class="text-center">LOREM IPSUM</span>
					<p class="text-center">CODER. DESIGNER. PROFESSIONAL</p>
				</a>
			</div>

			<div class="text-center">
				<ul class="social-links">
					<li><a href=""><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus-circle fa-2x" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-github fa-2x" aria-hidden="true"></i></a></li>
				</ul>
			</div>

			<div class="profile-pic-section">
				<figure class="text-center profile-pic">
					<img class="img-responsive" src="https://scontent.fblr2-1.fna.fbcdn.net/v/t1.0-0/p206x206/10004020_671685872890203_155942692_n.jpg?oh=a1ca1b2c9b8a9e580285f9436fade9cc&oe=58F0060A" alt="">
				</figure>
			</div>	
		</div>
	</section>

	<section class="about-me clearfix text-center">
		<h2 class="title-box-primary">About</h2>
		<h3 class="title-box-secondary">Personal Details</h3>
		<h3 class="title-box-sub-secondary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus cupiditate voluptas</h3>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut rerum maiores molestiae et deleniti tempora esse necessitatibus nesciunt, molestias aliquam voluptate nihil voluptatum corporis doloremque quisquam, omnis laboriosam, saepe cum.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut rerum maiores molestiae et deleniti tempora esse necessitatibus nesciunt, molestias aliquam voluptate nihil voluptatum corporis doloremque quisquam, omnis laboriosam, saepe cum.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut rerum maiores molestiae et deleniti tempora esse necessitatibus nesciunt, molestias aliquam voluptate nihil voluptatum corporis doloremque quisquam, omnis laboriosam, saepe cum.
		</p>

		<div class="container">
			<div class="row">
				<div class="col-xs-4">
					<figure>
						<div class="personal-details-icons">
							<i class="fa fa-star fa-3x"></i>
						</div>
						<figcaption class="personal-caption">Profile</figcaption>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ab ipsa rerum cumque, facilis quis est atque eligendi molestiae ut officiis possimus laboriosam! Doloribus dignissimos temporibus, dolorem, officia error consectetur.
						</p>
					</figure>
				</div>
				<div class="col-xs-4">
					<figure>
						<div class="personal-details-icons">
							<i class="fa fa-star fa-3x"></i>
						</div>
						<figcaption class="personal-caption">Address</figcaption>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ab ipsa rerum cumque, facilis quis est atque eligendi molestiae ut officiis possimus laboriosam! Doloribus dignissimos temporibus, dolorem, officia error consectetur.
						</p>
					</figure>
				</div>
				<div class="col-xs-4">
					<figure>
						<div class="personal-details-icons">
							<i class="fa fa-star fa-3x"></i>
						</div>
						<figcaption class="personal-caption">Hobbies And Interst</figcaption>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ab ipsa rerum cumque, facilis quis est atque eligendi molestiae ut officiis possimus laboriosam! Doloribus dignissimos temporibus, dolorem, officia error consectetur.
						</p>
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section class="skills clearfix text-center">
		<h2 class="title-box-primary">My Skills</h2>
		<h3 class="title-box-secondary">What I'm best at</h3>
		<div class="container">

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>

			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="skills-wrapper col-xs-11">
					<div class="desc">HTML/CSS</div>
					<div class="diagram">
						<span style="width: 50%">
							<em></em>
						</span>
					</div>
				</div>
				<div class="label col-xs-1">
					<span>78%</span>
				</div>				
			</div>			

		</div>
	</section>

	<section class="resume clearfix text-center">
		<h2 class="title-box-primary">Reusme</h2>
		<h3 class="title-box-secondary">My Entire Career</h3>
		<div class="container">
			<div class="col-sm-6 text-left">
				<h3 class="career-button">Education</h3>
				<div class="row border-right">
					<div class="col-xs-6 left-section">
						<span class="from-month">September</span> 
						<span class="to-month"></span>
						<hr class="horizontal-line">
						<h2>University Name</h2>
						<span>Course</span>
						<span class="marks"></span>
					</div>
					<div class="col-xs-6 right-section">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia explicabo, animi perferendis quidem quisquam accusantium est doloremque expedita. Velit quisquam quia excepturi dicta obcaecati ex, totam sunt autem iure necessitatibus!
					</div>
				</div>
			</div>
			<div class="col-sm-6 text-left">
				<h3 class="career-button">Work Experience</h3>
				<div class="row">
					<div class="col-xs-6 left-section">
						<span class="from-month">September</span> 
						<span class="to-month"></span>
						<hr class="horizontal-line">
						<h2>University Name</h2>
						<span>Course</span>
						<span class="marks"></span>
					</div>
					<div class="col-xs-6 right-section">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia explicabo, animi perferendis quidem quisquam accusantium est doloremque expedita. Velit quisquam quia excepturi dicta obcaecati ex, totam sunt autem iure necessitatibus!
					</div>
				</div>
			</div>			
		</div>
	</section>

	<section class="portfolio clearfix text-center">
		<h2 class="title-box-primary">portfolio</h2>
		<h3 class="title-box-secondary">My Latest Works</h3>
		<ul class="project-categories">
			<li>all</li>
			<li>Codeigniter</li>
			<li>Laravel</li>
			<li>Wordpress</li>
			<li>html</li>
		</ul>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
					<span class="zoom-icon"></span>
				</div>
				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
				</div>
				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
				</div>

				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
				</div>
				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
				</div>
				<div class="col-xs-12 col-sm-2 col-md-4 project">
					<img src="images/thumbnail.jpg" alt="" class="img-responsive project-thumbnail">
				</div>				
			</div>
		</div>
	</section>	

	<section class="my-work" style="height: 400px;">
		
	</section>
</div>

</body>
</html>